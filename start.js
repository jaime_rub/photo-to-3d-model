/////////////////////////////////////////////////////////////////////
// Copyright (c) Autodesk, Inc. All rights reserved
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted,
// provided that the above copyright notice appears in all copies and
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC.
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
/////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------
// These packages are included in package.json.
// Run `npm install` to install them.
// 'path' is part of Node.js and thus not inside package.json.
//-------------------------------------------------------------------

var express = require('express'),           // For web server
    multer = require('multer'),             // To handle file upload
    multerS3 = require('multer-s3');
var Axios = require('axios');               // A Promised base http client
var bodyParser = require('body-parser');    // Receive JSON format
var asyncLoop = require('node-async-loop');
// Set up Express web server
var app = express();
var bodyParser = require('body-parser');
app.use(express.static(__dirname + '/www'));

// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// This is for web server to start listening to port 3000
app.set('port', 3000);
var server = app.listen(app.get('port'), function () {
    console.log('Server listening on port ' + server.address().port);
});

//-------------------------------------------------------------------
// Configuration for AWS
//-------------------------------------------------------------------

const AWS = require('aws-sdk');
AWS.config.update({
    secretAccessKey: 'MnKPa5JViVxK0s9fBXtqf4+5pg/lpDkrth8WWmGn',
    accessKeyId: 'AKIAID2TUSFTM3GP66IA',
    region: 'ap-southeast-1',
});
const s3 = new AWS.S3({
    signatureVersion: 'v2',
});

//-------------------------------------------------------------------
// Configuration for Forge account
// Initialize the 2-legged OAuth2 client, and
// set specific scopes
//-------------------------------------------------------------------

var FORGE_CLIENT_ID = '8Rnh23Dx8cvaEUDYEXvFky8sGX2YX9vX' //process.env.FORGE_CLIENT_ID;
var FORGE_CLIENT_SECRET = 'xugxcFIEwAb4q0vO' //process.env.FORGE_CLIENT_SECRET;
var access_token = '';
var scopes = 'data:read data:write data:create bucket:create bucket:read';
const querystring = require('querystring');

//-------------------------------------------------------------------
// Routes
//-------------------------------------------------------------------

// Route /api/forge/oauth
app.get('/api/forge/oauth', function (req, res) {
    Axios({
        method: 'POST',
        url: 'https://developer.api.autodesk.com/authentication/v1/authenticate',
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
        },
        data: querystring.stringify({
			client_id: FORGE_CLIENT_ID,
            client_secret: FORGE_CLIENT_SECRET,
            grant_type: 'client_credentials',
            scope: scopes
        })
    })
        .then(function (response) {
            // Success
            access_token = response.data.access_token;
            console.log(response);
            res.redirect('/upload.html');
        })
        .catch(function (error) {
            // Failed
            console.log(error);
            res.send('Failed to authenticate');
        });
});

// Upload pictures to s3
var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: function (req, file, cb) {
            cb(null, "3d-modeling-h3/" + req.body.prefix);
        },
        acl: 'public-read',
        key: function (req, file, cb) {
            cb(null, file.originalname);
        }
    })
});
app.post('/api/forge/recap/photoscene/uploadfiles', upload.array('fileToUpload', 1000), function (req, res, next) {
    let tasksreportlist = [];
    var params = {
        Bucket: '3d-modeling-h3', 
        FetchOwner: true || false,
        MaxKeys: '1000',
        Prefix: req.body.prefix,
    };
    s3.listObjectsV2(params, function (err, data) {
    //    if (err) console.log(err, err.stack); // an error occurred
    //    else console.log(data.Contents); // successful response
        asyncLoop(data.Contents, function (files, cb) {
            tasksreportlist.push(files.Key)
            cb()
        }, function (err) {
            if (err) {
                console.error('Error: ' + err.message);
                return;
            }
 //           return res.status(200).json({ "files": tasksreportlist });
        });
    });
//    console.log('Done uploading images!');
    var nextLink = '/api/forge/recap/photoscene/add?prefix=' + params.Prefix;
    res.redirect(nextLink);
});

// Route /api/forge/recap/photoscene/add
// Creates and initializes a photoscene for reconstruction.
app.get('/api/forge/recap/photoscene/add', function (req, res) {
    Axios({
        method: 'POST',
        url: 'https://developer.api.autodesk.com/photo-to-3d/v1/photoscene',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        },
        data: querystring.stringify({
            scenename: 'myscenename',
            format: 'obj'
        })
    })
        .then(function (response) {
            // Success
            console.log(response);
            if (response.data.Error) {
                res.send(response.data.Error.msg);
            }
            var photosceneId = response.data.Photoscene.photosceneid;
            var prefix = req.query.prefix;
            var nextLink = '/api/forge/recap/photoscene/upload/' + photosceneId + '/' + prefix;
            res.redirect(nextLink);
        })
        .catch(function (error) {
            // Failed
            console.log(error);
            res.send('Failed to create a photoscene');
        });
});

// Route /api/forge/recap/photoscene/upload
// Adds one or more files to a photoscene.
app.get('/api/forge/recap/photoscene/upload/:photosceneid/:prefix', function (req, res) {
    var photosceneId = req.params.photosceneid;
    var params = {
        Bucket: '3d-modeling-h3', 
        FetchOwner: true || false,
        MaxKeys: '1000',
        Prefix: req.params.prefix,
    };
    async function postaxio(value, photosceneId){
        let res = await Axios({
            method: 'POST',
            url: 'https://developer.api.autodesk.com/photo-to-3d/v1/file',
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + access_token
            },
            data: {
                photosceneid: photosceneId,
                type: 'image'
            },
            transformRequest: [function (data, headers) {
                value.forEach(function (item, i) {
                    filename = 'file['+item.ind.toString()+']';
                    data[filename] = item.val;
                });
                console.log(data);
                return querystring.stringify(data);
            }]
        })
        return res;
    }
    let getUrls = new Promise((resolve, reject) => {
        s3.listObjectsV2(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
            } else {
                var allKeys = [];
                var href = this.request.httpRequest.endpoint.href;
                var bucketUrl = href + params.Bucket + '/';
                data.Contents.forEach(function (item, i) {
                    var fileUrl = bucketUrl + item.Key;
                    allKeys.push(fileUrl);
//                    console.log(fileUrl);
                });
                if (data.IsTruncated) {
                    params.ContinuationToken = data.NextContinuationToken;
                    console.log("get further list...");
                } 
                resolve(allKeys);
            }
        });
      })
    getUrls.then((response) => {
        console.log('Starting posting axios');
        async function postaxios() {
            const argsCopy = [].concat(response.map((val, ind) => ({ val, ind })));
            var i, j, temparray, chunk = 10;
            var listPromises = [];
            for (i = 0, j = argsCopy.length; i < j; i += chunk) {
                temparray = argsCopy.slice(i,i+chunk);
                listPromises.push(postaxio(temparray, photosceneId));
            }
            Promise.all(listPromises).then(() => {
                var nextLink = '/api/forge/recap/photoscene/process?photosceneid=' + photosceneId;
                res.send('<p></p><a href="' + nextLink + '">Generate mesh</a>');  
            })
            .catch((e) => {
                console.log(e);
            });
        }
        postaxios();
    });
});

// Route /api/forge/recap/photoscene/process
// Starts photoscene processing.
app.get('/api/forge/recap/photoscene/process', function (req, res) {
    var photosceneId = req.query.photosceneid;
    Axios({
        method: 'POST',
        url: 'https://developer.api.autodesk.com/photo-to-3d/v1/photoscene/' + photosceneId,
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + access_token
        }
    })
        .then(function (response) {
            // Success
            console.log(response);
            if (response.data.Error) {
                res.send(response.data.Error.msg);
            }
            var nextLink = '/api/forge/recap/photoscene/checkprogress/' + photosceneId + '/' + '0';
            res.redirect(nextLink);
        })
        .catch(function (error) {
            // Failed
            console.log(error);
            res.send('Failed to process files in photoscene');
        });
});

// Route /api/forge/recap/photoscene/checkprogress
// Returns the processing progress and status of a photoscene.
app.get('/api/forge/recap/photoscene/checkprogress/:photosceneid/:progress', function (req, res) {
    var photosceneId = req.params.photosceneid;
    var oldProgress = req.params.progress;
    Axios({
        method: 'GET',
        url: 'https://developer.api.autodesk.com/photo-to-3d/v1/photoscene/' + photosceneId + '/progress',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
    })
        .then(function (response) {
            // Success
            console.log(response);
            if (response.data.Error) {
                res.send(response.data.Error.msg);
            } 
            if (response.data.Photoscene && response.data.Photoscene.progressmsg == 'DONE') {
                var nextLink = '/api/forge/recap/photoscene/result?photosceneid=' + photosceneId;
                res.send('<p>Mesh generation process complete!</p><a href="' + nextLink + '">Download mesh</a>');
            } else {
                newProgress = response.data.Photoscene.progress;
                var nextLink = '/api/forge/recap/photoscene/checkprogress/' +  photosceneId + '/' + newProgress;
                res.send('<p>Mesh is being processed, this may take a while. Refresh <a href="' + nextLink + '">here </a> to update progress. Current progress: ' + newProgress + '%...</p>');
            }
        })
        .catch(function (error) {
            // Failed
            console.log(error);
            res.send('Failed to check progress of photoscene');
        });
});

// Route /api/forge/recap/photoscene/result
// Returns a time-limited HTTPS link to an output file of the specified format.
app.get('/api/forge/recap/photoscene/result', function (req, res) {
    var photosceneId = req.query.photosceneid;
    Axios({
        method: 'GET',
        url: 'https://developer.api.autodesk.com/photo-to-3d/v1/photoscene/' + photosceneId + '?format=obj',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
    })
        .then(function (response) {
            // Success
            console.log(response);
            if (response.data.Error) {
                res.send(response.data.Error.msg);
            }
            if (response.data.Photoscene && response.data.Photoscene.progressmsg == 'DONE') {
                var nextLink = '/api/forge/recap/photoscene/delete?photosceneid=' + photosceneId;
                res.send('<p>Success! This is the link to the mesh:</p><p>' + response.data.Photoscene.scenelink + '</p>'
                    + 'Would you like to <a href="' + nextLink + '">delete photoscene</a>?');
            } else {
                newProgress = response.data.Photoscene.progress;
                var nextLink = '/api/forge/recap/photoscene/checkprogress/' +  photosceneId + '/' + newProgress;
                res.send('<p>Mesh is being processed, this may take a while. Refresh <a href="' + nextLink + '">here </a> to update progress. Current progress: ' + newProgress + '%...</p>');
            }
            
        })
        .catch(function (error) {
            // Failed
            console.log(error);
            res.send('Failed to get result of photoscene');
        });
});

// Route /api/forge/recap/photoscene/delete
// Deletes a photoscene and its associated assets (images, output files, ...).
app.get('/api/forge/recap/photoscene/delete', function (req, res) {
    var photosceneId = req.query.photosceneid;
    Axios({
        method: 'DELETE',
        url: 'https://developer.api.autodesk.com/photo-to-3d/v1/photoscene/' + photosceneId,
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + access_token
        }
    })
        .then(function (response) {
            // Success
            console.log(response);
            if (response.data.Error) {
                res.send(response.data.Error.msg);
            }
            res.send('<p>Photoscene deleted!</p>');
        })
        .catch(function (error) {
            // Failed
            console.log(error);
            res.send('Failed to delete photoscene');
        });
});